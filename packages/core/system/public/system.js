'use strict';

angular.module('mean.system', ['ui.router', 'mean-factory-interceptor'])
    .run(['$rootScope','$location', function($rootScope, $location) {
      $rootScope.$on('$locationChangeStart', function(next, current) {
        var toUrl = $location.$$url;
        if(toUrl.endsWith('/')) {
          $location.path(toUrl.substr(0,toUrl.length - 1));
          return;
        }
      });
      $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
        var toPath = toState.url;
        toPath = toPath.replace(new RegExp('/', 'g'), '');
        toPath = toPath.replace(new RegExp(':', 'g'),'-');
        $rootScope.state = toPath;
        if($rootScope.state === '' ) {
          $rootScope.state = 'firstPage';
        }
      });
    }]);
