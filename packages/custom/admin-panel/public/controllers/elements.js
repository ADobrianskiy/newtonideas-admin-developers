/**
 * Created by Tania on 4/12/2015.
 */

'use strict';

/* jshint -W098 */
angular.module('mean.admin-panel')
    .controller('EditElementController', ['$scope', '$http', 'Global', 'AdminPanel',
        function($scope, $http, Global, AdminPanel) {
            $scope.global = Global;
            $scope.url = window.location.href;
            if(!$scope.url.endsWith('/')) $scope.url += '/';
            var urlParts =$scope.url.split('/');
            $scope.category_id = urlParts[4];
            $scope.element_id = urlParts[urlParts.length - 3];

            var refresh = function() {
                $http.get('/api/element-edit/' + $scope.element_id).success(function (response) {
                    $scope.element = response;
                    $scope.errorMsg = '';
                    $scope.name = response.name;
                });
                AdminPanel.publishableItems('element-publishable/').then(function(response){
                    $scope.publishable = response.data;
                });
            };
            refresh();

            $scope.saveElement = function(){
                if (!$scope.element.name || !$scope.element.z_index) {
                    $scope.errorMsg = "Please, enter name and z-index of this element";
                    return;
                }
                if ($scope.element.name.length > 16) {
                    $scope.errorMsg = "Please, enter name shorter than 16 symbols";
                    $scope.element.name = '';
                    return;
                }
                if (isNaN($scope.element.z_index)) {
                    $scope.errorMsg = "Please, enter z_index in numerals";
                    $scope.element.z_index = '';
                    return;
                }
                $http.post('api/element-edit/' + $scope.element_id, $scope.element).success(function(response){
                    refresh();
                    $scope.saved = response;
                });
            };

            $scope.unpublish = function(element){
                AdminPanel.changeStatus(element, 'element-unpublish').then(function(){
                    refresh();
                });
            };

            $scope.publish = function(element){
                AdminPanel.changeStatus(element, 'element-publish').then(function(){
                    refresh();
                });
            };

            $scope.hasPublishedElementVariations = function(id) {
                if($scope.publishable === undefined) return false;
                return $scope.publishable.indexOf(id) !== -1;
            };

            $scope.movable = function(element){
                AdminPanel.changeStatus(element, 'element-movable').then(function(){
                    refresh();
                });
            };

            $scope.immovable = function(element){
                AdminPanel.changeStatus(element, 'element-immovable').then(function(){
                    refresh();
                });
            }
        }
    ])
    .controller('ElementsController', ['$scope', '$http', 'Global', 'AdminPanel',
        function($scope, $http, Global, AdminPanel) {
            $scope.global = Global;
            $scope.url = window.location.href;
            if(!$scope.url.endsWith('/')) $scope.url += '/';
            var urlParts = $scope.url.split('/');
            $scope.product_id = urlParts[urlParts.length - 2];

            var refresh = function() {
                $http.get('/api/product/' + $scope.product_id).success(function (response) {
                    $scope.elements = response;
                    if (response.length == 0) $scope.emptyMsg = 'No elements defined';
                    $scope.element = '';
                    $scope.errorMsg = '';
                });
                $http.get('/api/product-name/' + $scope.product_id).success(function (response) {
                    $scope.product_name = response;
                });
                AdminPanel.publishableItems('element-publishable/').then(function(response){
                    $scope.publishable = response.data;
                });
            };
            refresh();

            $scope.addElement = function(){
                if ($scope.element.name === undefined) {
                    $scope.errorMsg = "Please, enter name of new element";
                    return;
                }
                if ($scope.element.name.length > 16) {
                    $scope.errorMsg = "Please, enter name shorter than 16 symbols";
                    $scope.element.name = '';
                    return;
                }
                if (isNaN($scope.element.z_index)) {
                    $scope.errorMsg = "Please, enter z_index in numerals";
                    $scope.element.z_index = '';
                    return;
                }
                $http.post('api/product/' + $scope.product_id, $scope.element).success(function(response){
                    $scope.emptyMsg = '';
                    refresh();
                    $scope.added = response;
                    $scope.removed = false;
                });
            };

            $scope.removeElement = function(id, name){
                if (confirm('Delete element \''+name+'\'?') === true) {
                    $http.delete('api/product/' + id).success(function(response){
                        refresh();
                        $scope.added = false;
                        $scope.removed = name;
                    });
                }
            };

            $scope.unpublish = function(element){
                AdminPanel.changeStatus(element, 'element-unpublish').then(function(){
                    refresh();
                });
            };

            $scope.publish = function(element){
                AdminPanel.changeStatus(element, 'element-publish').then(function(){
                    refresh();
                });
            };

            $scope.hasPublishedElementVariations = function(id) {
                if($scope.publishable === undefined) return false;
                return $scope.publishable.indexOf(id) !== -1;
            };

            $scope.movable = function(element){
                AdminPanel.changeStatus(element, 'element-movable').then(function(){
                    refresh();
                });
            };

            $scope.immovable = function(element){
                AdminPanel.changeStatus(element, 'element-immovable').then(function(){
                    refresh();
                });
            }
        }
    ]);

