'use strict';

/* jshint -W098 */
angular.module('mean.admin-panel',['ncy-angular-breadcrumb'])
    .controller('AdminPanelController', ['$scope', '$http', 'Global', 'AdminPanel',
        function($scope, $http, Global, AdminPanel) {
            $scope.global = Global;
            $scope.package = {
                name: 'admin-panel'
            };
        }
    ])
    .controller('UploadController', ['$scope', 'multipartForm',
        function($scope,multipartForm) {
            $scope.fileForm = {
                "upload-width": 150,
                "upload-height": 150
            };

            $scope.Submit = function(){
                var uploadUrl = "/api/upload-img"
                multipartForm.post(uploadUrl, $scope.fileForm, function(response){
                    console.log("Response fields" + response);
                    var fullSizeUrl = response['full-size'];
                    var thumbsUrl = response['thumbs'];
                    console.log("fullSizeUrl:" + fullSizeUrl);
                    console.log("thumbsUrl:" + thumbsUrl);

                })
            };
        }
    ]);
