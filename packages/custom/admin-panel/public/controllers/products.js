/**
 * Created by Tania on 4/12/2015.
 */

'use strict';

/* jshint -W098 */
angular.module('mean.admin-panel')
    .controller('EditProductController', ['$scope', '$http', 'multipartForm', 'Global', 'AdminPanel',
        function($scope, $http, multipartForm, Global, AdminPanel) {
            $scope.url = window.location.href;
            if(!$scope.url.endsWith('/')) $scope.url += '/';
            var urlParts =  $scope.url.split('/');
            $scope.product_id = urlParts[urlParts.length - 3];
            $scope.element_id = urlParts[urlParts.length - 4];
            $scope.fileForm = {
                "upload-width": 100,
                "upload-height": 100
            };

            var refresh = function() {
                $http.get('/api/product-edit/' + $scope.product_id).success(function (response) {
                    $scope.product = response;
                    $scope.errorMsg = '';
                    $scope.name = response.name;
                });
                AdminPanel.publishableItems('product-publishable/').then(function(response){
                    $scope.publishable = response.data;
                });
            };
            refresh();

            $scope.saveProduct = function(){
                if (!$scope.product.name) {
                    $scope.errorMsg = "Please, enter name of this product";
                    return;
                }
                if ($scope.product.name.length > 16) {
                    $scope.errorMsg = "Please, enter name shorter than 16 symbols";
                    $scope.product.name = '';
                    return;
                }
                if (!$scope.fileForm.fileData && $scope.product.icon) {
                    $http.post('api/product-edit/' + $scope.product_id, $scope.product).success(function(response){
                        refresh();
                        $scope.saved = response;
                    });
                    return;
                }
                if (!$scope.fileForm.fileData) {
                    $scope.errorMsg = "Please, upload icon for this product";
                    return;
                }
                var uploadUrl = "/api/upload-img"
                multipartForm.post(uploadUrl, $scope.fileForm, function(response){
                    $scope.product.icon = response['thumbs'];
                    $http.post('api/product-edit/' + $scope.product_id, $scope.product).success(function(response){
                        refresh();
                        $scope.saved = response;
                    });
                })
            };
            $scope.unpublish = function(product){
                AdminPanel.changeStatus(product, 'product-unpublish').then(function(){
                    refresh();
                });
            };

            $scope.publish = function(product){
                AdminPanel.changeStatus(product, 'product-publish').then(function(){
                    refresh();
                });
            };

            $scope.hasPublishedElements = function(id) {
                if($scope.publishable === undefined) return false;
                return $scope.publishable.indexOf(id) !== -1;
            }
        }
    ])
    .controller('ProductsController', ['$scope', '$http', 'Global', 'AdminPanel',
        function($scope, $http, Global, AdminPanel) {
            $scope.url = window.location.href;
            if(!$scope.url.endsWith('/')) $scope.url += '/';
            var urlParts = $scope.url.split('/');
            $scope.category_id = urlParts[urlParts.length - 2];

            var refresh = function() {
                $http.get('/api/category/' + $scope.category_id).success(function (response) {
                    $scope.products = response;
                    if (response.length == 0) $scope.emptyMsg = 'No products defined';
                    $scope.product = '';
                    $scope.errorMsg = '';
                });
                $http.get('/api/category-name/' + $scope.category_id).success(function (response) {
                    $scope.category_name = response;
                });
                AdminPanel.publishableItems('product-publishable/').then(function(response){
                    $scope.publishable = response.data;
                });
            };
            refresh();

            $scope.addProduct = function(){
                if ($scope.product.name === undefined) {
                    $scope.errorMsg = "Please, enter name of new product";
                    return;
                }
                if ($scope.product.name.length > 16) {
                    $scope.errorMsg = "Please, enter name shorter than 16 symbols";
                    $scope.product.name = '';
                    return;
                }
                $http.post('api/category/' + $scope.category_id, $scope.product).success(function(response){
                    $scope.emptyMsg = '';
                    refresh();
                    $scope.added = response;
                    $scope.removed = false;
                });
            };

            $scope.removeProduct = function(id, name){
                if (confirm('Delete product \''+name+'\'?') === true) {
                    $http.delete('api/category/' + id).success(function(response){
                        refresh();
                        $scope.added = false;
                        $scope.removed = name;
                    });
                }
            };

            $scope.unpublish = function(product){
                AdminPanel.changeStatus(product, 'product-unpublish').then(function(){
                    refresh();
                });
            };

            $scope.publish = function(product){
                AdminPanel.changeStatus(product, 'product-publish').then(function(){
                    refresh();
                });
            };

            $scope.hasPublishedElements = function(id) {
                if($scope.publishable === undefined) return false;
                return $scope.publishable.indexOf(id) !== -1;
            }
        }
    ]);

