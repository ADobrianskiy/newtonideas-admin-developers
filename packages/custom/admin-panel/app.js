'use strict';

/*
 * Defining the Package
 */
var Module = require('meanio').Module,
    qt   = require('quickthumb'),
    bodyParser = require('body-parser');

var AdminPanel = new Module('admin-panel');

/*
 * All MEAN packages require registration
 * Dependency injection is used to define required modules
 */
AdminPanel.register(function(app, auth, database) {
  app.use(qt.static(__dirname + '/assets/uploads'));
  app.use(bodyParser.urlencoded({ extended: false }))
  app.use(bodyParser.json());
  //We enable routing. By default the Package Object is passed to the routes
  AdminPanel.routes(app, auth, database);

  //We are adding a link to the main menu for all authenticated users
  /*AdminPanel.menus.add({
    title: 'adminPanel example page',
    link: 'adminPanel example page',
    roles: ['authenticated'],
    menu: 'main'
  });*/

  AdminPanel.menus.add({
    title: 'Categories',
    link: 'adminPanel Categories',
    roles: ['authenticated'],
    menu: 'main'
  });


  AdminPanel.aggregateAsset('css', 'adminPanel.css');
  AdminPanel.aggregateAsset('js', '../lib/angular-breadcrumb/dist/angular-breadcrumb.js');

  /**
    //Uncomment to use. Requires meanio@0.3.7 or above
    // Save settings with callback
    // Use this for saving data from administration pages
    AdminPanel.settings({
        'someSetting': 'some value'
    }, function(err, settings) {
        //you now have the settings object
    });

    // Another save settings example this time with no callback
    // This writes over the last settings.
    AdminPanel.settings({
        'anotherSettings': 'some value'
    });

    // Get settings. Retrieves latest saved settigns
    AdminPanel.settings(function(err, settings) {
        //you now have the settings object
    });
    */

  return AdminPanel;
});
