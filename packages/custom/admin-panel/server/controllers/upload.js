'use strict';

/**
 * Module dependencies.
 */
var formidable = require('formidable'),
    util = require('util'),
    fs   = require('fs-extra'),
    im = require('imagemagick');

module.exports = function(AdminPanel) {
    var currModule = AdminPanel.loadedmodule;
    /**
     * Directory to images uploading
     * @type {string}
     */
    var uploadsDir = currModule.name + "/public/assets/uploads/",
        fileLocation, hash;

    return {
        /**
         * Upload picture
         */
        uploadPicture: function(req, res, next) {
            var form = new formidable.IncomingForm();
            var height = 0,
                width = 0;

            form.on('fileBegin', function(fields, files) {
                var d = new Date();
                hash = d.getTime();
                fileLocation = uploadsDir + hash;
            });
            form.parse(req, function(err, fields, files) {
                if(err){
                    res.json({successful: false});
                }
                width = fields["upload-width"];
                height = fields["upload-height"];
                var result = {
                    "full-size": "http://" + req.headers.host + "/admin-panel/assets/uploads/" + hash + "/" + files.fileData.name
                };
                if(width && height) {
                    result.thumbs = "http://" + req.headers.host + "/admin-panel/assets/uploads/" + hash + "/thumbs/"  + files.fileData.name;

                }
                res.json(result);
            });

            form.on('end', function(fields, files) {

                /* Temporary location of our uploaded file */
                var temp_path = this.openedFiles[0].path;
                /* The file name of the uploaded file */
                var file_name = this.openedFiles[0].name;

                /* Location where we want to copy the uploaded file */
                var fullSizeLocation = currModule.source + "/" + fileLocation + "/" + file_name;
                var thumbsLocation = currModule.source + "/" + fileLocation + "/thumbs/" + file_name;

                console.log(temp_path);
                console.log(thumbsLocation);
                fs.copy(temp_path, fullSizeLocation, function(err) {
                    if (err) {
                        if (err) throw err;
                    }
                });

                if(height && width) {
                    fs.copy(temp_path, thumbsLocation, function(err) {
                        if (err) {
                            console.error(err);
                        } else {
                            /*im.resize({
                                srcData: fs.readFileSync(thumbsLocation, 'binary'),
                                dstPath: thumbsLocation,
                                width:   width,
                                height: height
                            }, function(err, stdout, stderr){
                                if (err) console.log(err);
                            });*/
                        }
                    });
                }
            });
        },

        /**
         * Upload icon
         */
        uploadIcon: function(req, res, next) {
            var form = new formidable.IncomingForm();
            var height = 0,
                width = 0;

            form.on('fileBegin', function(fields, files) {
                var d = new Date();
                hash = d.getTime();
                fileLocation = uploadsDir + hash;
            });
            form.parse(req, function(err, fields, files) {
                if(err){
                    res.json({successful: false});
                }
                width = fields["upload-width"];
                height = fields["upload-height"];
                var result = {
                    "thumbs": "http://" + req.headers.host + "/admin-panel/assets/uploads/" + hash + "/thumbs/"  + files.fileData.name
                };
                res.json(result);
            });

            form.on('end', function(fields, files) {

                /* Temporary location of our uploaded file */
                var temp_path = this.openedFiles[0].path;
                /* The file name of the uploaded file */
                var file_name = this.openedFiles[0].name;

                /* Location where we want to copy the uploaded file */
                var thumbsLocation = currModule.source + "/" + fileLocation + "/thumbs/" + file_name;

                if(height && width) {
                    fs.copy(temp_path, thumbsLocation, function(err) {
                        if (err) {
                            console.error(err);
                        } /*else {
                            im.resize({
                                srcPath: thumbsLocation,
                                dstPath: thumbsLocation,
                                width:   width,
                                height: height
                            }, function(err, stdout, stderr){
                                if (err) console.log(err);
                            });
                        }*/
                    });

                }
            });
        }
    };
}
