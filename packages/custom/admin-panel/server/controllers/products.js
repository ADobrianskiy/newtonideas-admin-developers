/**
 * Created by Tania on 16/11/2015.
 */

'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Category = mongoose.model('Category'),
    Product = mongoose.model('Product');

var icon = require('./../defaultImage.json');

module.exports = function(AdminPanel) {
    return {
        /**
         * Show all products in list
         */
        showAll: function(req, res, next) {
            Product.find( function(err, doc) {
                if (err) return next(err);
                res.json(doc);
            });
        },
        /**
         * Show all products in list by category_id
         */
        show: function(req, res, next) {
            Product.find({ category_id: req.params.category_id }, function(err, doc) {
                if (err) return next(err);
                res.json(doc);
            });
        },

        /**
         * Add new product to current category
         */
        add: function(req, res, next) {
            var product = new Product({
                category_id: req.params.category_id,
                name: req.body.name,
                status: 'unpublished',
                icon: icon.link
            });
            product.save(function(err, doc) {
                if (err) return next(err);
                res.json(doc);
            });
        },

        /**
         * Remove product from current category
         */
        remove: function(req, res, next) {
            Product.remove({_id: req.params.id}, function(err, doc) {
                if (err) return next(err);
                res.json(doc);
            });
        },

        /**
         * * Set status in database to 'unpublished'
         * */
        unpublish: function(req, res, next) {
            Product.findOne({ _id: req.body._id }, function (err, doc){
                if (err) return next(err);
                doc.status = 'unpublished';
                doc.save();
                res.json(doc);
            });
        },

        /**
         * * Set status in database to 'published'
         * */
        publish: function(req, res, next) {
            Product.findOne({ _id: req.body._id }, function (err, doc){
                if (err) return next(err);
                doc.status = 'published';
                doc.save();
                res.json(doc);
            });
        },


        /**
         * Return product name by id
         */
        name: function(req, res, next) {
            Product.findOne({ _id: req.params.product_id }, function (err, doc){
                if (err) return next(err);
                res.json(doc.name);
            });
        },

        /**
         * Return categories that can be published
         */
        checkPublished: function(req, res, next) {
            var publishable = [];
            Category.find(function(err, doc) {
                console.log("there");
                if (err) return next(err);
                if(!doc || doc.length == 0) res.json(publishable);
                var i = 0;
                doc.forEach(function(item) {
                    Product.findOne({ category_id: item._id, status: 'published' }, function(err, prod) {
                        if (err) return next(err);
                        if (prod !== null) publishable.push(item._id);
                        i++;
                        if(i === doc.length){
                            res.json(publishable);
                        }
                    });
                });

            });
        },

        /**
         * Return product by id
         */
        product: function(req, res, next) {
            Product.findOne({ _id: req.params.product_id }, function (err, doc){
                if (err) return next(err);
                res.json(doc);
            });
        },

        /**
         * Edit product
         */
        editProduct: function(req, res, next) {
            Product.findOne({ _id: req.params.product_id }, function (err, doc){
                if (err) return next(err);
                doc.name = req.body.name;
                doc.icon = req.body.icon;
                doc.save();
                res.json(doc);
            });
        }
    };
}
