/**
 * Created by Tania on 24/11/2015.
 */

'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Element = mongoose.model('Element'),
    ElementVariation = mongoose.model('ElementVariation');

var icon = require('./../defaultImage.json');

module.exports = function(AdminPanel) {
    return {
        /**
         * Show all element variations in list
         */
        show: function(req, res, next) {
            ElementVariation.find({ element_id: req.params.element_id }, function(err, doc) {
                if (err) return next(err);
                res.json(doc);
            });
        },

        /**
         * Add new element variation to current element
         */
        add: function(req, res, next) {
            var elementVariation = new ElementVariation({
                element_id: req.params.element_id,
                name: req.body.name,
                price: req.body.price,
                status: 'unpublished',
                shown_by_default: 'hidden',
                icon: icon.link,
                picture: icon.link
            });
            elementVariation.save(function(err, doc) {
                if (err) return next(err);
                res.json(doc);
            });
        },

        /**
         * Remove element variation from current element
         */
        remove: function(req, res, next) {
            ElementVariation.remove({_id: req.params.id}, function(err, doc) {
                if (err) return next(err);
                res.json(doc);
            });
        },

        /**
         * * Set status in database to 'unpublished'
         * */
        unpublish: function(req, res, next) {
            ElementVariation.findOne({ _id: req.body._id }, function (err, doc){
                if (err) return next(err);
                doc.status = 'unpublished';
                doc.save();
                res.json(doc);
            });
        },

        /**
         * * Set status in database to 'published'
         * */
        publish: function(req, res, next) {
            ElementVariation.findOne({ _id: req.body._id }, function (err, doc){
                if (err) return next(err);
                doc.status = 'published';
                doc.save();
                res.json(doc);
            });
        },

        /**
         * * Set shown by default status in database to 'shown'
         * */
        shown: function(req, res, next) {
            ElementVariation.find({ element_id: req.body.element_id }, function (err, doc){
                if (err) return next(err);
                doc.forEach(function(item) {
                    item.shown_by_default = 'hidden';
                    if (item._id == req.body._id)
                        item.shown_by_default = 'shown';
                    item.save();
                })
                res.json(doc);
            });
        },

        /**
         * * Set shown by default status in database to 'hidden'
         * */
        hidden: function(req, res, next) {
            ElementVariation.findOne({ _id: req.body._id }, function (err, doc){
                if (err) return next(err);
                doc.shown_by_default = 'hidden';
                doc.save();
                res.json(doc);
            });
        },

        /**
         * Return elements that can be published
         */
        checkPublished: function(req, res, next) {
            var publishable = [];
            Element.find(function(err, doc) {
                if (err) return next(err);
                if(!doc || doc.length == 0)res.json(publishable);
                var i = 0;
                doc.forEach(function(item) {
                    ElementVariation.findOne({ element_id: item._id, status: 'published' }, function(err, prod) {
                        if (err) return next(err);
                        if (prod !== null) publishable.push(item._id);
                        i++
                        if(i === doc.length){
                            res.json(publishable);
                        }
                    });
                })
            });
        },

        /**
         * Return element variation by id
         */
        elementVariation: function(req, res, next) {
            ElementVariation.findOne({ _id: req.params.elementVariation_id }, function (err, doc){
                if (err) return next(err);
                res.json(doc);
            });
        },

        /**
         * Edit element variation
         */
        editElementVariation: function(req, res, next) {
            ElementVariation.findOne({ _id: req.params.elementVariation_id }, function (err, doc){
                if (err) return next(err);
                doc.name = req.body.name;
                if (req.body.icon) {
                    doc.icon = req.body.icon;
                }
                if (req.body.picture) {
                    doc.picture = req.body.picture;
                }
                doc.description = req.body.description;
                doc.price = req.body.price;
                doc.save();
                res.json(doc);
            });
        }
    };
}
