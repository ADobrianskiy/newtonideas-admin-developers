/**
 * Created by Tania on 23/10/2015.
 */

'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Category = mongoose.model('Category'),
    Product = mongoose.model('Product');

var icon = require('./../defaultImage.json');

module.exports = function(AdminPanel) {
    return {
        /**
         * Show all categories in list
         */
        show: function(req, res, next) {
            Category.find(function(err, categories) {
                if (err) return next(err);
                res.json(categories);
            });
        },

        /**
         * Add new category to database
         */
        add: function(req, res, next) {
            var cat = new Category({
                name: req.body.name,
                status: 'unpublished',
                icon: icon.link
            });
            cat.save(function(err, cat) {
                if (err) return next(err);
                res.json(cat);
            });
        },

        /**
         * Remove category from database
         */
        remove: function(req, res, next) {
            var id = req.params.id;
            Category.remove({_id: id}, function(err, categories) {
                if (err) return next(err);
                Product.remove({category_id: id}, function(err, doc) {
                    if (err) return next(err);
                    res.json(categories);
                });
            });

        },

        /**
         * * Set status in database to 'unpublished'
         * */
        unpublish: function(req, res, next) {
            Category.findOne({ _id: req.body._id }, function (err, doc){
                if (err) return next(err);
                doc.status = 'unpublished';
                doc.save();
                res.json(doc);
            });
        },

        /**
         * * Set status in database to 'published'
         * */
        publish: function(req, res, next) {
            Category.findOne({ _id: req.body._id }, function (err, doc){
                if (err) return next(err);
                doc.status = 'published';
                doc.save();
                res.json(doc);
            });
        },

        /**
         * Return the category name by id
         */
        name: function(req, res, next) {
            Category.findOne({ _id: req.params.category_id }, function (err, doc){
                if (err) return next(err);
                res.json(doc.name);
            });
        },

        /**
         * Return category by id
         */
        category: function(req, res, next) {
            Category.findOne({ _id: req.params.category_id }, function (err, doc){
                if (err) return next(err);
                res.json(doc);
            });
        },

        /**
         * Edit category
         */
        editCategory: function(req, res, next) {
            Category.findOne({ _id: req.params.category_id }, function (err, doc){
                if (err) return next(err);
                doc.name = req.body.name;
                doc.icon = req.body.icon;
                doc.save();
                res.json(doc);
            });
        }
    };
}
