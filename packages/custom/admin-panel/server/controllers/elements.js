/**
 * Created by Tania on 24/11/2015.
 */

'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Element = mongoose.model('Element'),
    Product = mongoose.model('Product');

module.exports = function(AdminPanel) {
    return {
        /**
         * Show all elements in list
         */
        show: function(req, res, next) {
            Element.find({ product_id: req.params.product_id }, function(err, doc) {
                if (err) return next(err);
                res.json(doc);
            });
        },

        /**
         * Add new element to current product
         */
        add: function(req, res, next) {
            var element = new Element({
                product_id: req.params.product_id,
                name: req.body.name,
                status: 'unpublished',
                movability: 'immovable',
                z_index: req.body.z_index
            });
            element.save(function(err, doc) {
                if (err) return next(err);
                res.json(doc);
            });
        },

        /**
         * Remove element from current product
         */
        remove: function(req, res, next) {
            Element.remove({_id: req.params.id}, function(err, doc) {
                if (err) return next(err);
                res.json(doc);
            });
        },

        /**
         * * Set status in database to 'unpublished'
         * */
        unpublish: function(req, res, next) {
            Element.findOne({ _id: req.body._id }, function (err, doc){
                if (err) return next(err);
                doc.status = 'unpublished';
                doc.save();
                res.json(doc);
            });
        },

        /**
         * * Set status in database to 'published'
         * */
        publish: function(req, res, next) {
            Element.findOne({ _id: req.body._id }, function (err, doc){
                if (err) return next(err);
                doc.status = 'published';
                doc.save();
                res.json(doc);
            });
        },

        /**
         * * Set movability in database to 'movable'
         * */
        movable: function(req, res, next) {
            Element.findOne({ _id: req.body._id }, function (err, doc){
                if (err) return next(err);
                doc.movability = 'movable';
                doc.save();
                res.json(doc);
            });
        },

        /**
         * * Set movability in database to 'immovable'
         * */
        immovable: function(req, res, next) {
            Element.findOne({ _id: req.body._id }, function (err, doc){
                if (err) return next(err);
                doc.movability = 'immovable';
                doc.save();
                res.json(doc);
            });
        },

        /**
         * Return element name by id
         */
        name: function(req, res, next) {
            Element.findOne({ _id: req.params.element_id }, function (err, doc){
                if (err) return next(err);
                res.json(doc.name);
            });
        },

        /**
         * Return products that can be published
         */
        checkPublished: function(req, res, next) {
            var publishable = [];
            Product.find(function(err, doc) {
                if (err) return next(err);
                if(!doc || doc.length == 0) res.json(publishable);
                var i = 0;
                doc.forEach(function(item) {
                    Element.findOne({ product_id: item._id, status: 'published' }, function(err, prod) {
                        if (err) return next(err);
                        if (prod !== null) publishable.push(item._id);
                        i++;
                        if(i === doc.length){
                            res.json(publishable);
                        }
                    });
                })
            });
        },


        /**
         * Return element by id
         */
        element: function(req, res, next) {
            Element.findOne({ _id: req.params.element_id }, function (err, doc){
                if (err) return next(err);
                res.json(doc);
            });
        },

        /**
         * Edit element
         */
        editElement: function(req, res, next) {
            Element.findOne({ _id: req.params.element_id }, function (err, doc){
                if (err) return next(err);
                doc.name = req.body.name;
                doc.z_index = req.body.z_index;
                doc.save();
                res.json(doc);
            });
        }
    };
}
