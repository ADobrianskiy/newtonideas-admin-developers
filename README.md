# LIST OF TECHNOLOGIES #
1. JavaScript [codeacedemy](https://www.codecademy.com/en/tracks/javascript)
2. MEAN stack:
     * M - Mongodb [youtube](https://www.youtube.com/watch?v=tgckAOyjXPI)
     * E - Express [expressjs.com](http://expressjs.com/ru/guide/routing.html)
     * A - Angular [codeacedemy](https://www.codecademy.com/en/courses/learn-angularjs)
     * N - Node.js [nodebeginner.ru](http://www.nodebeginner.ru/)
3. LESS [lesscss.org](http://lesscss.org/)
4. Gulp [tutorial](https://scotch.io/tutorials/automate-your-tasks-easily-with-gulp-js)

# GET STARTED #

## PREREQUISITE TECHNOLOGIES##

1. Node.js - you can find it [here](https://nodejs.org/en/).
2. MongoDB - you can download it [here](https://www.mongodb.org/downloads#production).
3. Git - [here](http://git-scm.com/downloads) is link. The path to the installed folder should be in PATH variable
4. Python v2.5 - 2.7 - [download page](https://www.python.org/downloads/). After installing create PYTHON variable. 

Reboot your PC or laptop 

## GLOBAL MODULES ##

1. Install gulp: Type "npm install -g gulp" in command prompt. (sudo necessary for ubuntu)
2. Install bower: Type "npm install -g bower" in command prompt.(sudo necessary for ubuntu)

## CLONE & CONFIGURE ##
### You can skip first 2 steps if you feel that you can work with git in other way ###
1. Download and install [SourceTree](https://www.sourcetreeapp.com/).
2. Open SourceTree, log in and clone project.
3. Run IDE (WebStorm, Intellij Idea) and select the project. After launching open terminal (press alt+f12) and type "npm install". (also you can run this command in command prompt, but you should be in the project root)
4. After that type "bower install"n run this command in command prompt, but you should be in the project root)
5. Go to packages/custom/admin-panel and type "bower install" again
6. Try to create a new user. If errors present, add "body-parser" to express server(edit node_modules/meanio/lib/core_modules/server/ExpressEngine.js file)
 

## Running ##
1. Run mongodb
2. Open terminal in IDE or command prompt inside the project folder and type "gulp"