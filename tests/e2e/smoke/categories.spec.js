
describe('Categories', function(){


    var util = require('util');
    var path = require("path");
    var consts = require('../../resources/Constants.js');
   // var tools = require('../../e2e/Tools.js');
   

  it('should add a category successfully', function(){
   /* 
   1. Precondition: admin should be logged in
   2. Steps to be executed: 
      1) enter a name of the category
      2) click on the "Add" button
   3. Expected result: one more category is 
      added to the categories list
	*/
      browser.get('/');
      protractor.tools.login();
      browser.get('/categories', 4000);//.then(
      //  function () {
      element(by.id('txt-input-name')).sendKeys(protractor.consts.testCategory_categoryName);
      element(by.id('category-add')).click();
      browser.driver.wait(protractor.until.elementLocated(by.css('.alert-success')));
      expect(element(by.id(protractor.consts.testCategory_categoryName)).isDisplayed()).toBeTruthy();
      // }
      //  );
      // });
  });

 //
 it('should fail adding a category without entering its name', function(){
 //  /*
 //  1. Precondition: admin should be logged in
 //  2. Steps to be executed:
 //     1) click on the "Add" button
 //  3. Expected result: proper error must be
 //     displayed and prompt to enter a name
	//*/
     protractor.tools.login();

     browser.get('/categories', 4000);
     element(by.id('category-add')).click();
     browser.driver.wait(protractor.until.elementLocated(by.css('.alert-danger')));
     expect(element(by.css('.alert-danger')));
 });
 //


 it('should cancel removing a category', function(){
  /*
  1. Precondition:
     1) admin should be logged in
     2) at least one category should be in the list
  2. Steps to be executed:
     1) click on the "Remove" button of the respective category
	 2) click on the "Cancel" button on the popup window
  3. Expected result: a respective category isn't removed
  */
     protractor.tools.login();
     browser.get('/categories', 4000);
     element(by.id('category-remove-' + protractor.consts.testCategory_categoryName)).click();
     browser.switchTo().alert().dismiss();
     expect(element(by.id(protractor.consts.testCategory_categoryName)).isDisplayed()).toBeTruthy();
 });
 it('should edit a category successfully without changes', function(){
   /*
	1. Precondition:
	  1) admin should be logged in
      2) at least one category should be in the list
   2. Steps to be executed:
	  1) click on the "Edit" button of the respective category
	  2) click on the "Save" button
   3. Expected result: a respective category is saved without changes
	  and admin is navigated back to the categories list page
  */
     protractor.tools.login();
     browser.get('/categories', 4000);
     element(by.id('category-edit-' + protractor.consts.testCategory_categoryName)).click();
     var fileToUpload = '../../resources/' + protractor.consts.testCategory_image,
         absolutePath = path.resolve(__dirname, fileToUpload);

     element(by.id('upload-file')).sendKeys(absolutePath);
     //browser.driver.wait(protractor.until.elementLocated(by.id('category-save')));
     element(by.id('category-save')).click();
     browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
     element(by.css('.btn-success')).click();
     browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testCategory_categoryName)));
     expect(browser.getCurrentUrl()).toMatch(/.*categories.*/);

 });

 it('should edit a category successfully, changing its name', function(){
   /*
	1. Precondition:
	  1) admin should be logged in
      2) at least one category should be in the list
   2. Steps to be executed:
	  1) click on the "Edit" button of the respective category
	  2) edit the name
	  2) click on the "Save" button
   3. Expected result: a respective category's name is edited
  */
     protractor.tools.login();
     browser.get('/categories', 4000);
     element(by.id('category-edit-' + protractor.consts.testCategory_categoryName)).click();
     browser.driver.wait(protractor.until.elementLocated(by.id('category-save')));
     //TODO: relocate all const vars to external js file
     element(by.id('txt-input-name')).clear();
     element(by.id('txt-input-name')).sendKeys(consts.testCategory_categoryRename);
     element(by.id('category-save')).click();
     browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
     element(by.css('.btn-success')).click();
     browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testCategory_categoryRename)));
     expect(element(by.id(protractor.consts.testCategory_categoryRename)).isPresent()).toBeTruthy();


     browser.get('/categories', 4000);
     element(by.id('category-edit-' + protractor.consts.testCategory_categoryRename)).click();
     browser.driver.wait(protractor.until.elementLocated(by.id('category-save')));
     element(by.id('txt-input-name')).clear();
     element(by.id('txt-input-name')).sendKeys(protractor.consts.testCategory_categoryName);
     element(by.id('category-save')).click();
     browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
     element(by.css('.btn-success')).click();
     browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testCategory_categoryName)));
 });

 it('should edit a category successfully, changing its icon', function(){
   /*
	1. Precondition:
	  1) admin should be logged in
      2) at least one category should be in the list
   2. Steps to be executed:
	  1) click on the "Edit" button of the respective category
	  2) click on the "Choose Files" button
	  3) choose the appropriate folder
	  4) choose the appropriate png image
	  5) click on the "Open" button
	  6) click on the "Save" button
   3. Expected result: a respective category's icon is edited
  */
     protractor.tools.login();
     browser.get('/categories', 4000);
     element(by.id('category-edit-' + protractor.consts.testCategory_categoryName)).click();
     var fileToUpload = '../../resources/' +  protractor.consts.testCategory_editImage,
         absolutePath = path.resolve(__dirname, fileToUpload);

     element(by.id('upload-file')).sendKeys(absolutePath);
     browser.driver.wait(protractor.until.elementLocated(by.id('category-save')));
     element(by.id('category-save')).click();
     browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
     expect(element(by.css('.btn-success')).isPresent()).toBeTruthy();

 });

  it('should return to the categories list after saving category-s edition', function(){
 /*
	1. Precondition:
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) "Save" button should be clicked on on the edition page
	2. Steps to be executed:
	   1) Click the "Ok" button on the popup message
	3. Expected result: admin is navigated back 
	   to the categories list page
	*/
     browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
     element(by.css('.btn-success')).click();
     browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testCategory_categoryName)));
     expect(browser.getCurrentUrl()).toMatch(/.*categories.*/);
 });
   // TODO: ensures to revert the changes after this test case
  it('should make the category published', function(){
   /*
	1. Precondition:
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) at least one product should be in the list
	   4) at least one element should be in the list
       5) at least one element variation should be in the list	   
	   6) at least one of its products should be published
	   7) the category should be unpublished
	2. Steps to be executed:
	   1) click on the "Status" checkmark of the respective category
	3. Expected result: the respective category gets published status
	*/
      protractor.tools.login();
      browser.get('/categories', 4000);
      element(by.id('publish-'+protractor.consts.testCategory_pub_unpub_category)).click();
      expect(element(by.id('published-'+protractor.consts.testCategory_pub_unpub_category)).isDisplayed()).toBeTruthy();
  });

  it('should make the category unpublished', function(){
   /*
	1. Precondition:
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) at least one product should be in the list
	   4) at least one element should be in the list
       5) at least one element variation should be in the list
	   6) at least one of its products should be published
	   7) the category should be published
	2. Steps to be executed:
	   1) click on the "Status" checkmark of the respective category
	3. Expected result: the respective category gets unpublished status
	*/
      protractor.tools.login();
      browser.get('/categories', 4000);
      element(by.id('unpublish-'+protractor.consts.testCategory_pub_unpub_category)).click();
      expect(element(by.id('unpublished-'+protractor.consts.testCategory_pub_unpub_category)).isDisplayed()).toBeTruthy();
 });
   it('should remove a category successfully', function(){
       /*
        1. Precondition:
        1) admin should be logged in
        2) at least one category should be in the list
        2. Steps to be executed:
        1) click on the "Remove" button of the respective category
        2) click on the "Ok" button on the popup window
        3. Expected result: a respective category is removed
        */
       protractor.tools.login();
       browser.get('/categories', 4000);
       element(by.id('category-remove-' + protractor.consts.testCategory_categoryName)).click();
       browser.switchTo().alert().accept();
       browser.driver.wait(protractor.until.elementLocated(by.css('.alert-success')));
       expect(element(by.id(protractor.consts.testCategory_categoryName)).isPresent()).toBeFalsy();
   });

 
});
 
  