describe('Products', function(){

    var util = require('util');
    var path = require("path");
    var consts = require('../../resources/Constants.js');

    beforeAll(function () {
       //protractor.tools.setupData();
    });

  it('should add a product successfully', function(){
   /* 
   1. Precondition: 
      1) admin should be logged in
	  2) at least one category should be in the list
   2. Steps to be executed: 
      1) enter a name of the product
      2) click on the "Add" button
   3. Expected result: one more product is 
      added to the products list
	*/

      browser.get('/');
      login();
      getToCategory();

      element(by.id('txt-input-name')).sendKeys(protractor.consts.testProduct_prodName);
      element(by.id('product-add')).click();
      browser.driver.wait(protractor.until.elementLocated(by.css('.alert-success')));
      expect(element(by.id(protractor.consts.testProduct_prodName)).isDisplayed()).toBeTruthy();

  });
  
  it('should fail adding a product without entering its name', function(){
   /* 
   1. Precondition: 
      1) admin should be logged in
	  2) at least one category should be in the list
   2. Steps to be executed: 
      1) click on the "Add" button
   3. Expected result: proper error must be
      displayed and prompt to enter a name
	*/
      login();

      getToCategory();
      element(by.id('product-add')).click();
      browser.driver.wait(protractor.until.elementLocated(by.css('.alert-danger')));
      expect(element(by.css('.alert-danger')).isDisplayed()).toBeTruthy();
  });
  

  
  it('should cancel removing a product', function(){
   /* 
   1. Precondition: 
      1) admin should be logged in
	  2) at least one category should be in the list
	  3) at least one product should be in the list
   2. Steps to be executed:
      1) click on the "Remove" button of the respective product
	  2) click on the "Cancel" button on the popup window
   3. Expected result: a respective product isn't removed
   */
      login();
      getToCategory();
      element(by.id('product-remove-' + protractor.consts.testProduct_prodName)).click();
      browser.switchTo().alert().dismiss();
      expect(element(by.id(protractor.consts.testProduct_prodName)).isDisplayed()).toBeTruthy();
  });
  
  it('should edit a product successfully without changes', function(){
    /*
	1. Precondition: 
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) at least one product should be in the list
    2. Steps to be executed:
	   1) click on the "Edit" button of the respective product
	   2) click on the "Save" button
    3. Expected result: a respective product is saved without changes
	   and admin is navigated back to the products list page
   */
      login();
      getToCategory();
      element(by.id('product-edit-' + protractor.consts.testProduct_prodName)).click();
      var fileToUpload = '../../resources/' + protractor.consts.testProduct_image,
          absolutePath = path.resolve(__dirname, fileToUpload);

      element(by.id('upload-file')).sendKeys(absolutePath);
     // browser.driver.wait(protractor.until.elementLocated(by.id('product-save')));
      element(by.id('product-save')).click();
      browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
      element(by.css('.btn-success')).click();
      browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testProduct_prodName)));
      expect(element(by.id('product-add')).isPresent()).toBeTruthy();
  });

  it('should edit a product successfully, changing its name', function(){
    /*
	1. Precondition: 
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) at least one product should be in the list
    2. Steps to be executed:
	   1) click on the "Edit" button of the respective product
	   2) edit the name
	   2) click on the "Save" button
    3. Expected result: a respective product's name is edited
    */
      login();
      getToCategory();
      element(by.id('product-edit-' + protractor.consts.testProduct_prodName)).click();
      browser.driver.wait(protractor.until.elementLocated(by.id('product-save')));
      element(by.id('txt-input-name')).clear();
      element(by.id('txt-input-name')).sendKeys(consts.testProduct_prodRename);
      element(by.id('product-save')).click();
      browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
      element(by.css('.btn-success')).click();
      browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testProduct_prodRename)));
      expect(element(by.id(protractor.consts.testProduct_prodRename)).isPresent()).toBeTruthy();


      element(by.id('product-edit-' + protractor.consts.testProduct_prodRename)).click();
      browser.driver.wait(protractor.until.elementLocated(by.id('product-save')));
      element(by.id('txt-input-name')).clear();
      element(by.id('txt-input-name')).sendKeys(protractor.consts.testProduct_prodName);
      element(by.id('product-save')).click();
      browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
      element(by.css('.btn-success')).click();
      browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testProduct_prodName)));
  });

  it('should edit a product successfully, changing its icon', function(){
    /*
	1. Precondition: 
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) at least one product should be in the list
    2. Steps to be executed:
	   1) click on the "Edit" button of the respective product
	   2) click on the "Choose Files" button
	   3) choose the appropriate folder
	   4) choose the appropriate png image
	   5) click on the "Open" button
	   6) click on the "Save" button
    3. Expected result: a respective product's icon is edited
    */

      login();
      getToCategory();
      element(by.id('product-edit-' + protractor.consts.testProduct_prodName)).click();
      var fileToUpload = '../../resources/' +  protractor.consts.testProduct_editImage,
          absolutePath = path.resolve(__dirname, fileToUpload);

      element(by.id('upload-file')).sendKeys(absolutePath);
      browser.driver.wait(protractor.until.elementLocated(by.id('product-save')));
      element(by.id('product-save')).click();
      browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
      expect(element(by.css('.btn-success')).isPresent()).toBeTruthy();
   });

   it('should return to the products list after saving a product-s edition', function(){
   /*
	1. Precondition:
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) at least one product should be in the list
       4) "Save" button should be clicked on on the edition page	   
	2. Steps to be executed:
	   1) Click the "Ok" button on the popup message
	3. Expected result: admin is navigated back 
	   to the products list page
	*/
       browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
       element(by.css('.btn-success')).click();
       browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testProduct_prodName)));
       expect(element(by.id(protractor.consts.testProduct_prodName)).isPresent()).toBeTruthy();
   });
  
   it('should make the product published', function(){
    /*
	1. Precondition:
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) at least one product should be in the list
	   4) at least one element should be in the list
       5) at least one element variation should be in the list	   
	   6) at least one of its elements should be published
	   7) the product should be unpublished
	2. Steps to be executed:
	   1) click on the "Status" checkmark of the respective product
	3. Expected result: the respective product gets published status, 
	   therefore its category gets possibility to get published
	*/
       login();
       getToCategory();
       element(by.id('publish-'+protractor.consts.testProduct_pub_unpub_prod)).click();
       expect(element(by.id('published-'+protractor.consts.testProduct_pub_unpub_prod)).isDisplayed()).toBeTruthy();
   });

   it('should make the product unpublished', function(){
    /*
	1. Precondition:
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) at least one product should be in the list
	   4) at least one element should be in the list
       5) at least one element variation should be in the list	   
	   6) at least one of its elements should be published
	   7) the product should be published
	2. Steps to be executed:
	   1) click on the "Status" checkmark of the respective element 
	3. Expected result: the respective product gets unpublished status
	*/
       login();
       getToCategory();
       element(by.id('unpublish-'+protractor.consts.testProduct_pub_unpub_prod)).click();
       expect(element(by.id('unpublished-'+protractor.consts.testProduct_pub_unpub_prod)).isDisplayed()).toBeTruthy();
  });

    it('should remove a product successfully', function(){
        /*
         1. Precondition:
         1) admin should be logged in
         2) at least one category should be in the list
         3) at least one product should be in the list
         2. Steps to be executed:
         1) click on the "Remove" button of the respective product
         2) click on the "Ok" button on the popup window
         3. Expected result: a respective product is removed
         */
        login();
        getToCategory();
        element(by.id('product-remove-' + protractor.consts.testProduct_prodName)).click();
        browser.switchTo().alert().accept();
        browser.driver.wait(protractor.until.elementLocated(by.css('.alert-success')));
        expect(element(by.id(protractor.consts.testProduct_prodName)).isPresent()).toBeFalsy();
    });

    function login()
    {

        browser.get(browser.baseUrl + "/auth/login");
        element(by.id('log-in')).isDisplayed().then(function(result) {
            if ( result ) {
                browser.get("/auth/login");
                element(by.id('email_block')).sendKeys('admin@admin.ua');
                element(by.id('password_block')).sendKeys('123456789');
                element(by.css('.submit_button')).click();
                browser.driver.wait(protractor.until.elementLocated(By.css('.btn-success')));
            } else {
                //Whatever if it is false (not displayed)
            }
        });
    }
    function getToCategory()
    {
        browser.get('/categories');
        element(by.id(protractor.consts.testProduct_category)).click();
        browser.driver.wait(protractor.until.elementLocated(by.id('product-add')));
    }

});