describe('Element Variations', function(){
    var path = require("path");
  it('should add an element variation successfully', function(){
   /* 
   1. Precondition: 
      1) admin should be logged in
	  2) at least one category should be in the list
	  3) at least one product should be in the list
	  4) at least one element should be in the list
   2. Steps to be executed: 
      1) enter a name of the element variation
	  2) enter a price in numerals
      3) click on the "Add" button
   3. Expected result: one more product is 
      added to the elements list
   */
      browser.get('/');
      protractor.tools.login();
      getToElement();
      element(by.id('txt-input-name')).sendKeys(protractor.consts.testVariation_varName);
      element(by.id('txt-input-price')).sendKeys(protractor.consts.defaultPrice);
      element(by.id('elVariation-add')).click();
      browser.driver.wait(protractor.until.elementLocated(by.css('.alert-success')));
      expect(element(by.id(protractor.consts.testVariation_varName)).isDisplayed()).toBeTruthy();
  });
    
  it('should fail adding an element variation without entering its name', function(){
   /* 
   1. Precondition: 
      1) admin should be logged in
	  2) at least one category should be in the list
	  3) at least one product should be in the list
	  4) at least one element should be in the list
   2. Steps to be executed: 
	  1) enter a price in numerals
      2) click on the "Add" button
   3. Expected result: proper error must be
      displayed and prompt to enter a name
   */
      protractor.tools.login();
      getToElement();
      element(by.id('txt-input-price')).sendKeys(protractor.consts.defaultPrice);
      element(by.id('elVariation-add')).click();
      browser.driver.wait(protractor.until.elementLocated(by.css('.alert-danger')));
      expect(element(by.id(protractor.consts.testVariation_varName)).isDisplayed()).toBeTruthy();

  });
  
  it('should fail adding an element variation without entering its price', function(){
   /* 
   1. Precondition: 
      1) admin should be logged in
	  2) at least one category should be in the list
	  3) at least one product should be in the list
	  4) at least one element should be in the list
   2. Steps to be executed: 
      1) enter a name of the element variation
      2) click on the "Add" button
   3. Expected result: proper error must be
      displayed and prompt to enter the price
   */
      protractor.tools.login();
      getToElement();
      element(by.id('txt-input-name')).sendKeys(protractor.consts.testVariation_varName);
      element(by.id('elVariation-add')).click();
      browser.driver.wait(protractor.until.elementLocated(by.css('.alert-danger')));
      expect(element(by.id(protractor.consts.testVariation_varName)).isDisplayed()).toBeTruthy();
  });
  
   it('should fail adding an element variation without entering any information', function(){
   /* 
   1. Precondition: 
      1) admin should be logged in
	  2) at least one category should be in the list
	  3) at least one product should be in the list
	  4) at least one element should be in the list
   2. Steps to be executed:
      1) click on the "Add" button
   3. Expected result: proper error must be
      displayed and prompt to enter a name
   */
       protractor.tools.login();
       getToElement();
       element(by.id('elVariation-add')).click();
       browser.driver.wait(protractor.until.elementLocated(by.css('.alert-danger')));
       expect(element(by.id(protractor.consts.testVariation_varName)).isDisplayed()).toBeTruthy();

   });
  

  
  it('should cancel removing an element variation', function(){
   /* 
   1. Precondition: 
      1) admin should be logged in
	  2) at least one category should be in the list
	  3) at least one product should be in the list
	  4) at least one element should be in the list
	  5) at least one element variation should be in the list
   2. Steps to be executed: 
      1) click on the "Remove" button of the respective element variation
      2) click on the "Cancel" button on the popup window
   3. Expected result: a respective element variation isn't removed
   */
      protractor.tools.login();
      getToElement();
      element(by.id('elVariation-remove-' + protractor.consts.testVariation_varName)).click();
      browser.switchTo().alert().dismiss();
      expect(element(by.id(protractor.consts.testVariation_varName)).isDisplayed()).toBeTruthy();
  });
  
  it('should edit an element variation successfully, changing its name', function(){
    /*
	1. Precondition: 
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) at least one product should be in the list
	   4) at least one element should be in the list
	   5) at least one element variation should be in the list
    2. Steps to be executed:
	   1) click on the "Edit" button of the respective element variation
	   2) edit its name
	   3) click on the "Save" button
    3. Expected result: a respective element variation's name is edited
    */
      protractor.tools.login();
      getToElement();
      element(by.id('elVariation-edit-' + protractor.consts.testVariation_varName)).click();
      browser.driver.wait(protractor.until.elementLocated(by.id('elVariation-save')));
      //browser.pause(5000);
      var fileToUpload = '../../resources/' + protractor.consts.testElement_image,
      	absolutePath = path.resolve(__dirname, fileToUpload);

      element(by.id('upload-file')).sendKeys(absolutePath);
      browser.driver.wait(protractor.until.elementLocated(by.id('elVariation-save')));

      element(by.id('txt-input-name')).clear();
      element(by.id('txt-input-name')).sendKeys(protractor.consts.testVariation_varRename);

      element(by.id('description')).clear();
      element(by.id('description')).sendKeys(protractor.consts.defaultDescr);

      element(by.id('elVariation-save')).click();
      browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
      element(by.css('.btn-success')).click();
      browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testVariation_varRename)));
      expect(element(by.id(protractor.consts.testVariation_varRename)).isPresent()).toBeTruthy();


      element(by.id('elVariation-edit-' + protractor.consts.testVariation_varRename)).click();
      browser.driver.wait(protractor.until.elementLocated(by.id('elVariation-save')));
      element(by.id('txt-input-name')).clear();
      element(by.id('txt-input-name')).sendKeys(protractor.consts.testVariation_varName);
      element(by.id('elVariation-save')).click();
      browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
      element(by.css('.btn-success')).click();
      browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testVariation_varName)));
      browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testVariation_varName)));

  });
   
   it('should edit an element variation successfully, changing its price', function(){
    /*
	1. Precondition:
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) at least one product should be in the list
	   4) at least one element should be in the list
	   5) at least one element variation should be in the list
    2. Steps to be executed:
	   1) click on the "Edit" button of the respective element variation
	   2) edit its price (in numerals)
	   3) click on the "Save" button
    3. Expected result: a respective element variation's price is edited
    */
       protractor.tools.login();
       getToElement();
       element(by.id('elVariation-edit-' + protractor.consts.testVariation_varName)).click();
       browser.driver.wait(protractor.until.elementLocated(by.id('elVariation-save')));

       element(by.id('description')).clear();
       element(by.id('description')).sendKeys(protractor.consts.defaultDescr);

       element(by.id('price')).clear();
       element(by.id('price')).sendKeys(protractor.consts.defaultPrice);

       element(by.id('elVariation-save')).click();
       browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
       element(by.css('.btn-success')).click();
       browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testVariation_varName)));
       expect(element(by.id(protractor.consts.testVariation_varName)).isPresent()).toBeTruthy();

   });
    
   it('should edit an element variation successfully, changing its description', function(){
    /*
	1. Precondition: 
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) at least one product should be in the list
	   4) at least one element should be in the list
	   5) at least one element variation should be in the list
    2. Steps to be executed:
	   1) click on the "Edit" button of the respective element
	   2) edit its description
	   3) click on the "Save" button
    3. Expected result: a respective element variation's description is edited
    */
       protractor.tools.login();
       getToElement();
       element(by.id('elVariation-edit-' + protractor.consts.testVariation_varName)).click();
       browser.driver.wait(protractor.until.elementLocated(by.id('elVariation-save')));

       element(by.id('description')).clear();
       element(by.id('description')).sendKeys(protractor.consts.defaultDescr);

       element(by.id('elVariation-save')).click();
       browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
       element(by.css('.btn-success')).click();
       browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testVariation_varName)));
       expect(element(by.id(protractor.consts.testVariation_varName)).isPresent()).toBeTruthy();

   });

    it('should edit an element variation successfully, changing its icon', function(){
    /*
	1. Precondition: 
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) at least one product should be in the list
	   4) at least one element should be in the list
	   5) at least one element variation should be in the list
    2. Steps to be executed:
	   1) click on the "Edit" button of the respective element variation
	   2) click on the "Choose Files" button
	   3) choose the appropriate folder
	   4) choose the appropriate png image
	   5) click on the "Open" button
	   6) click on the "Save" button
    3. Expected result: a respective element variation's icon is edited
    */
        protractor.tools.login();
        getToElement();
        element(by.id('elVariation-edit-' + protractor.consts.testVariation_varName)).click();
        browser.driver.wait(protractor.until.elementLocated(by.id('elVariation-save')));
        var fileToUpload = '../../resources/' + protractor.consts.testElement_editImage,
            absolutePath = path.resolve(__dirname, fileToUpload);
        element(by.id('upload-file')).sendKeys(absolutePath);

        element(by.id('elVariation-save')).click();
        browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
        element(by.css('.btn-success')).click();
        browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testVariation_varName)));
        expect(element(by.id(protractor.consts.testVariation_varName)).isPresent()).toBeTruthy();

    });
  
  it('should edit an element variation successfully, changing its picture', function(){
    /*
	1. Precondition: 
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) at least one product should be in the list
	   4) at least one element should be in the list
	   5) at least one element variation should be in the list
    2. Steps to be executed:
	   1) click on the "Edit" button of the respective element variation
	   2) click on the "Choose Files" button
	   3) choose the appropriate folder
	   4) choose the appropriate png image
	   5) click on the "Open" button
	   6) click on the "Save" button
    3. Expected result: a respective element variation's picture is edited
    */
      protractor.tools.login();
      getToElement();
      element(by.id('elVariation-edit-' + protractor.consts.testVariation_varName)).click();
      browser.driver.wait(protractor.until.elementLocated(by.id('elVariation-save')));
      var fileToUpload = '../../resources/' + protractor.consts.testElement_editImage,
          absolutePath = path.resolve(__dirname, fileToUpload);
      element(by.id('upload-picture')).sendKeys(absolutePath);

      element(by.id('elVariation-save')).click();
      browser.driver.wait(protractor.until.elementLocated(by.css('.btn-success')));
      element(by.css('.btn-success')).click();
      browser.driver.wait(protractor.until.elementLocated(by.id(protractor.consts.testVariation_varName)));
      expect(element(by.id(protractor.consts.testVariation_varName)).isPresent()).toBeTruthy();

  });
   
  it('should make the element variation published', function(){
    /*
	1. Precondition:
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) at least one product should be in the list
	   4) at least one element should be in the list
       5) at least one element variation should be in the list	   
	   6) the element should be unpublished
	2. Steps to be executed:
	   1) click on the "Status" checkmark of the respective element variation 
	3. Expected result: the respected element variation gets published status, 
	   therefore its element gets possibility to get published
	*/
      protractor.tools.login();
      getToElement();
      element(by.id('publish-'+protractor.consts.testVariation_varName)).click();
      expect(element(by.id('published-'+protractor.consts.testVariation_varName)).isDisplayed()).toBeTruthy();

  });

  it('should make the element variation unpublished', function(){
    /*
	1. Precondition:
	   1) admin should be logged in
       2) at least one category should be in the list
	   3) at least one product should be in the list
	   4) at least one element should be in the list
       5) at least one element variation should be in the list	
	   6) the element should be published
	2. Steps to be executed:
	   1) click on the "Status" checkmark of the respective element variation 
	3. Expected result: the respective element variation gets unpublished status
	*/
      protractor.tools.login();
      getToElement();
      element(by.id('unpublish-'+protractor.consts.testVariation_varName)).click();
      expect(element(by.id('unpublished-'+protractor.consts.testVariation_varName)).isDisplayed()).toBeTruthy();

  });

    it('should remove an element variation successfully', function(){
        /*
         1. Precondition:
         1) admin should be logged in
         2) at least one category should be in the list
         3) at least one product should be in the list
         4) at least one element should be in the list
         5) at least one element variation should be in the list
         2. Steps to be executed:
         1) click on the "Remove" button of the respective element variation
         2) click on the "Ok" button on the popup window
         3. Expected result: a respective element variation is removed
         */
        protractor.tools.login();
        getToElement();
        element(by.id('elVariation-remove-' + protractor.consts.testVariation_varName)).click();
        browser.switchTo().alert().accept();
        browser.driver.wait(protractor.until.elementLocated(by.css('.alert-success')));
        expect(element(by.id(protractor.consts.testVariation_varName)).isPresent()).toBeFalsy();

    });
	function getToElement() {
		browser.get('/categories');
		element(by.id(protractor.consts.testVariation_category)).click();
		browser.driver.wait(protractor.until.elementLocated(by.id('product-add')));
		element(by.id(protractor.consts.testVariation_product)).click();
		browser.driver.wait(protractor.until.elementLocated(by.id('element-add')));
		element(by.id(protractor.consts.testVariation_element)).click();
		browser.driver.wait(protractor.until.elementLocated(by.id('elVariation-add')));
	}
});