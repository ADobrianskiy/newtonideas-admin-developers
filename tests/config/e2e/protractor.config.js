var jasmineReporters = require('jasmine-reporters');
var Jasmine2HtmlReporter = require('protractor-jasmine2-html-reporter');


exports.config = {
  baseUrl: 'http://localhost:3001',
  framework: 'jasmine2',
  specs: [
    '../../e2e/**/*.spec.js'
  ],
  multiCapabilities: [
    {
      browserName: 'chrome'
    //},
    //{
    //  browserName: 'firefox'
    }
  ],

  onPrepare: function(){
    //Creates independent results files for each browser
    //Otherwise they run at the same time and overwrite each other
    var capsPromise = browser.getCapabilities();
    protractor.consts = require('../../resources/Constants.js');
    protractor.tools = require('../e2e/Tools.js');
    return capsPromise.then(function(caps){
      var browserName = caps.caps_? caps.caps_.browserName : "undefined browser name";
      var browserVersion = caps.caps_? caps.caps_.version : "undefined browser version";
      var browserPrefix = browserName + '-' + browserVersion + '-';
      jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
        savePath: 'tests/results/e2e/junit',
        filePrefix: browserPrefix,
        consolidateAll: false
      }));
      jasmine.getEnv().addReporter(
          new Jasmine2HtmlReporter({
            savePath: 'tests/results/e2e/html/'
          })
      );
    });

  }

};
