/**
 * Created by Alex on 3/16/2016.
 */
exports.test_init = ["Category", "Product", "Element", "Variation"];
exports.testCategory_categories = ["Test1"];
exports.testCategory_products = [["Prod1"]];
exports.testCategory_elements = [[["Element1"]]];
exports.testCategoty_variations = [[[["Var1"]]]];
exports.testCategory_defaultPrice = "1";
exports.testCategory_defaultZindex = "1";
exports.testCategory_categoryRename = "Test1Rename";