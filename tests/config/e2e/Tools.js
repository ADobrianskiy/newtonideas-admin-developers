/**
 * Created by Alex on 3/16/2016.
 */

exports.login = function() {

    browser.get(browser.baseUrl + "/auth/login");
    element(by.id('log-in')).isDisplayed().then(function (result) {
        if (result) {
            browser.get("/auth/login");
            element(by.id('email_block')).sendKeys('admin@admin.ua');
            element(by.id('password_block')).sendKeys('123456789');
            element(by.css('.submit_button')).click();
            browser.driver.wait(protractor.until.elementLocated(By.css('.btn-success')));
        }
    });
}

exports.getToCategories = function()
{
    browser.get('/categories');
    element(by.id(protractor.consts.testProduct_category)).click();
    browser.driver.wait(protractor.until.elementLocated(by.id('category-add')));
}

exports.getToCategory = function(name)
{
    exports.getToCategories();
    element(by.id(name)).click();
    browser.driver.wait(protractor.until.elementLocated(by.id('product-add')));
}

exports.getToProduct = function (catName, prodname) {
    exports.getToCategory(catName);
    element(by.id(prodname)).click();
    browser.driver.wait(protractor.until.elementLocated(by.id('element-add')));
}

exports.getToElement = function (catName, prodname, elname) {
    exports.getToProduct(catName, prodname);
    element(by.id(elname)).click();
    browser.driver.wait(protractor.until.elementLocated(by.id('elVariation-add')));
}

exports.addCategory = function (name) {

    exports.getToCategories();
    
    // element(by.id(name)).isDisplayed().then(function (result) {
    //     if (result) {
    //
    //     }
    //     else
    //     {
    //         console.log("******WOW");
            element(by.id('txt-input-name')).sendKeys(name);
            element(by.id('category-add')).click();
            browser.driver.wait(protractor.until.elementLocated(By.id(name)));
    //    }
   // });
}

exports.addProduct = function (cat, name) {
    exports.getToCategory(cat);
   // element(by.id(name)).isDisplayed().then(function (result) {
   //     if (result) {
            element(by.id('txt-input-name')).sendKeys(name);
            element(by.id('product-add')).click();
           // browser.driver.wait(protractor.until.elementLocated(By.id(name)));
      //  }
   // });
}

exports.addElement = function (cat, prod, name, z_index) {
    exports.getToProduct(cat, prod);
    //element(by.id(name)).isDisplayed().then(function (result) {
   //     if (result) {
            element(by.id('txt-input-name')).sendKeys(name);
            element(by.id('z-index')).sendKeys(z_index);
            element(by.id('element-add')).click();
            browser.driver.wait(protractor.until.elementLocated(By.id(name)));
    //    }
   // });
}

exports.addVariation = function (cat, prod, el, name, price) {
    exports.getToElement(cat, prod, el);
   // element(by.id(name)).isDisplayed().then(function (result) {
   //     if (result) {
            element(by.id('txt-input-name')).sendKeys(name);
            element(by.id('txt-input-price')).sendKeys(price);
            element(by.id('elVariation-add')).click();
            browser.driver.wait(protractor.until.elementLocated(By.id(name)));
    //    }
   // });
}


exports.pubCategory = function (name, value) {
    exports.getToCategories();
    exports.publishCurrent(name, value);
}

exports.pubProd = function (cat, name, value) {
    exports.getToCategory(cat);
    exports.publishCurrent(name, value);
}

exports.pubElem = function (cat, prod, name, value) {
    exports.getToProduct(cat, prod);
    exports.publishCurrent(name, value);
}

exports.pubVar = function (cat, prod, elem, name, value) {
    exports.getToElement(cat, prod, elem);
    exports.publishCurrent(name, value);
}



exports.publishCurrent = function (name, value) {
    if(value)
    {
        element(by.id('publish-' + name)).isDisplayed().then(function (result) {
            if (result) {

                element(by.id('publish-' + name)).click();
                browser.driver.wait(protractor.until.elementLocated(By.id('published-' + name)));
            }
        });
    }
    else
    {
        element(by.id('unpublish-' + name)).isDisplayed().then(function (result) {
            if (result) {

                element(by.id('unpublish-' + name)).click();
                browser.driver.wait(protractor.until.elementLocated(By.id('unpublished-' + name)));
            }
        });
    }
}

exports.dataIsSetUp = 0;

exports.setupData = function()
{
    if(exports.dataIsSetUp == 0) {
        exports.dataIsSetUp = 1;
        exports.login();
        console.log("******1");
        exports.addCategory(protractor.consts.testCategory_pub_unpub_category);
        browser.driver.wait(protractor.until.elementLocated(By.id(protractor.consts.testCategory_pub_unpub_category)));
        console.log("******2");
        exports.addProduct(protractor.consts.testCategory_pub_unpub_category, protractor.consts.genericName);
        console.log("******3");
       // browser.pause(10000);
        exports.addElement(protractor.consts.testCategory_pub_unpub_category, protractor.consts.genericName, protractor.consts.genericName, protractor.consts.defaultZindex);
        console.log("******4");
        exports.addVariation(protractor.consts.testCategory_pub_unpub_category, protractor.consts.genericName, protractor.consts.genericName, protractor.consts.genericName, protractor.consts.defaultPrice);
        exports.pubVar(protractor.consts.testCategory_pub_unpub_category, protractor.consts.genericName, protractor.consts.genericName, protractor.consts.genericName, "1");
        exports.pubElem(protractor.consts.testCategory_pub_unpub_category, protractor.consts.genericName, protractor.consts.genericName, "1");
        exports.pubProd(protractor.consts.testCategory_pub_unpub_category, protractor.consts.genericName, "1");
       // exports.pubCategory(protractor.consts.testCategory_pub_unpub_category, "1");

    }
}

// exports.init = function()
// {
//
//     if(protractor.consts.test_init)
//     {
//         exports.addCategory(protractor.consts.test_init[0], protractor.consts.test_init[1], protractor.consts.test_init[2], protractor.consts.test_init[3], true);
//     }
// }
//
// exports.initTestCategories = function() {
//     return new Promise(function(parentResolve, reject){
//         exports.login();
//         var promises = [];
//         for(i = 0; i < protractor.consts.testCategory_categories.length; i++)
//         {
//             promises.push(exports.addCategory(protractor.consts.testCategory_categories[i], protractor.consts.testCategory_products[i],
//                 protractor.consts.testCategory_elements[i], protractor.consts.testCategoty_variations[i], true));
//         }
//         Promise.all(promises).then(function(){
//             parentResolve();
//         })
//     });
//
//
//
// }
//
// exports.removeTestCategories = function()
// {
//     exports.login();
//     browser.get('/categories');
//     for(i = 0; i < protractor.consts.testCategory_categories.length; i++)
//     {
//        exports.removeCategory(protractor.consts.testCategory_categories[i]);
//     }
// }
//
// exports.removeCategory = function(name)
// {
//     browser.get('/categories');
//     element(by.id('category-remove-' + name)).click();
//     browser.switchTo().alert().accept();
//     browser.driver.wait(protractor.until.elementLocated(by.css('.alert-success')));
// }
//
// exports.addCategory = function(name, products, elements, variations, publishAll) {
//     return new Promise(function(resolve, reject) {
//         browser.get('/categories').then(function(){
//             element.all(by.id(name)).then(function (items) {
//                 if (items.length == 0) {
//                     element(by.id('txt-input-name')).sendKeys(name);
//                     element(by.id('category-add')).click();
//                     //browser.driver.wait(protractor.until.elementLocated(by.id(name)));
//                 }
//             });
//
//             var startingUrlPromise = browser.getCurrentUrl();
//
//             startingUrlPromise.then(function (startingUrl) {
//                 browser.controlFlow().execute(function () {
//                     if (products.length > 0) {
//                         for (i = 0; i < products.length; i++) {
//                             browser.get(startingUrl);
//                             element(by.id(name)).click();
//                             browser.driver.wait(protractor.until.elementLocated(by.id('product-add')));
//                             exports.addProduct(products[i], elements[i], variations[i], publishAll);
//                             browser.get(startingUrl);
//                             if (publishAll) {
//                                 element(by.id('publish-' + products[i])).click();
//                                 browser.driver.wait(protractor.until.elementLocated(by.id('published-' + products[i])));
//                             }
//                         }
//                     }
//                     if (publishAll) {
//                         browser.get('/categories');
//                         element(by.id('publish-' + name)).click();
//                         browser.driver.wait(protractor.until.elementLocated(by.id('published-' + name)));
//                     }
//                 });
//                 resolve();
//             });
//         });
//
//
//     });
// };
//
// exports.addProduct = function(name, elements, variations, publishAll) {
//     element.all(by.id('name')).then(function(items) {
//         if(items.length == 0)
//         {
//             element(by.id('txt-input-name')).sendKeys(name);
//             element(by.id('product-add')).click();
//             browser.driver.wait(protractor.until.elementLocated(by.id(name)));
//         }
//     });
//
//     var startingUrlPromise = browser.getCurrentUrl();
//     startingUrlPromise.then(function (startingUrl) {
//         if (elements) {
//             for (i = 0; i < elements.length; i++) {
//                 browser.get(startingUrl);
//                 element(by.id(name)).click();
//                 browser.driver.wait(protractor.until.elementLocated(by.id('element-add')));
//                 exports.addElement(elements[i], variations[i], publishAll);
//                 browser.get(startingUrl);
//                 if (publishAll) {
//                     element(by.id('publish-' + elements[i])).click();
//                     browser.driver.wait(protractor.until.elementLocated(by.id('published-' + elements[i])));
//                 }
//             }
//         }
//     });
// }
//
// exports.addElement = function(name, variations, publishAll)
// {
//
//     element.all(by.id('name')).then(function(items) {
//         if(items.length == 0)
//         {
//             element(by.id('txt-input-name')).sendKeys(name);
//             element(by.id('z-index')).sendKeys(exports.testCategory_defaultZindex);
//             element(by.id('product-add')).click();
//             browser.driver.wait(protractor.until.elementLocated(by.id(name)));
//         }
//     });
//     var startingUrl = browser.getCurrentUrl();
//     startingUrl.then(function (resolved) {
//         if (variations) {
//             for (i = 0; i < variations.length; i++) {
//                 browser.get(startingUrl);
//                 element(by.id(name)).click();
//                 browser.driver.wait(protractor.until.elementLocated(by.id('elVariation-add')));
//                 exports.addVariation(variations[i], publishAll);
//                 browser.get(startingUrl);
//                 if (publishAll) {
//                     element(by.id('publish-' + variations[i])).click();
//                     browser.driver.wait(protractor.until.elementLocated(by.id('published-' + variations[i])));
//                 }
//             }
//         }
//     });
//
// }
//
// exports.addVariation = function(name, publishAll)
// {
//
//     element.all(by.id('name')).then(function(items) {
//         if(items.length == 0)
//         {
//             element(by.id('txt-input-price')).sendKeys(exports.testCategory_defaultPrice);
//             element(by.id('txt-input-name')).sendKeys(name);
//             element(by.id('product-add')).click();
//             browser.driver.wait(protractor.until.elementLocated(by.id(name)));
//         }
//     });
//     if(publishAll)
//     {
//         element(by.id('publish-' + name)).click();
//         browser.driver.wait(protractor.until.elementLocated(by.id('published-' + name)));
//     }
// }