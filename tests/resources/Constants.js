/**
 * Created by Alex on 3/21/2016.
 */
exports.test_init = ["Category", "Product", "Element", "Variation"];
exports.testCategory_categories = ["Test1"];
exports.testCategory_products = [["Prod1"]];
exports.testCategory_elements = [[["Element1"]]];
exports.testCategoty_variations = [[[["Var1"]]]];
exports.testCategory_defaultPrice = "1";
exports.testCategory_defaultZindex = "1";

exports.genericName = "Generic";
exports.defaultPrice = "1";
exports.defaultZindex = "1";
exports.defaultDescr = "Descr";

exports.testCategory_categoryName = "NewCategory";
exports.testCategory_categoryRename = "NewCategoryRen";
exports.testCategory_pub_unpub_category = "PublishTestCat";
exports.testCategory_image = "koala.png";
exports.testCategory_editImage = "Penguins.png";

exports.testProduct_category = "TestingProduct";
exports.testProduct_prodName = "NewProduct";
exports.testProduct_pub_unpub_prod = "PublishTestProd";
exports.testProduct_prodRename = "NewProductRen";
exports.testProduct_image = "koala.png";
exports.testProduct_editImage = "Penguins.png";

exports.testElement_category = "TestingProduct";
exports.testElement_product = "TestingElement";
exports.testElement_elemName = "NewElement";
exports.testElement_pub_unpub_el = "PublishTestEl";
exports.testElement_elemRename = "NewElemRen";
exports.testElement_image = "koala.png";
exports.testElement_editImage = "Penguins.png";

exports.testVariation_category = "TestingProduct";
exports.testVariation_product = "TestingElement";
exports.testVariation_element = "TestingVar";
exports.testVariation_varName = "NewVar";
exports.testVariation_pub_unpub_var = "PublishTestVar";
exports.testVariation_varRename = "NewVarRen";
exports.testVariation_image = "koala.png";
exports.testVariation_editImage = "Penguins.png";